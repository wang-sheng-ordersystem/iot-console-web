import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        loading: false
    },
    mutations: {
        startLoading(){
            this.state.loading=true;
        },
        endLoading(){
            this.state.loading=false;
        }
    }
})